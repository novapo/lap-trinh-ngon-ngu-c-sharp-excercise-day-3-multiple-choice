﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Xml.XPath;
using System.Diagnostics;

namespace bai2cs
{

    public partial class Dialog_AddResultFile : Form
    {

        private string result_file_content = "";
        FileStream fs;
        OpenFileDialog ofd = new OpenFileDialog();
        public string ResultFileContent
        {
            get { return result_file_content; }
            set { result_file_content = value; }
        }

        public Dialog_AddResultFile()
        {
            InitializeComponent();
        }

        private void ButtonAgree_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void ButtonChooseFile_Click(object sender, EventArgs e)
        {

            if (ofd.ShowDialog() == DialogResult.OK)
            {
                fs = new FileStream(ofd.FileName, FileMode.OpenOrCreate);
                StreamReader sr = new StreamReader(fs);
                result_file_content = sr.ReadToEnd();

                TextBoxFilePath.Text = ofd.FileName;

                

                fs.Close();
            };


            //result_file_content = "hello";
        }
    }
}
