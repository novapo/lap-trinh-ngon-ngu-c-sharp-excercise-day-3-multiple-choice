﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;

namespace bai2cs
{
    public partial class tracnghiem : Form
    {
        private DataTable AnswerTable = new DataTable();

        public string result_data = "";
        public tracnghiem()
        {
            InitializeComponent();
        }

        private void button1_Read(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                FileStream fs = new FileStream(ofd.FileName, FileMode.OpenOrCreate);
                StreamReader sr = new StreamReader(fs);
                string content = sr.ReadToEnd();
                TextBoxContent.Text = content;
                textBoxfilename.Text = ofd.SafeFileName;
                textBoxfilepath.Text = ofd.FileName;
                sr.Close();
            }
        }

        private void tracnghiem_Load(object sender, EventArgs e)
        {
            DataGridViewAnswerTable.Columns.Clear();
            AnswerTable.Columns.Add("Question", typeof(int));
            AnswerTable.Columns.Add("Answer", typeof(string));
            AnswerTable.Columns.Add("Result", typeof(string));
            for (int i = 1; i <= 50; i++) AnswerTable.Rows.Add(i, "", "");
            DataGridViewAnswerTable.DataSource = AnswerTable;
        }

        private void ButtonCheckAnswer_Click(object sender, EventArgs e)
        {
            Dialog_AddResultFile dialog = new Dialog_AddResultFile();

            TextBoxContent.AppendText(DataGridViewAnswerTable.Rows[50].Cells[0].Value + "");
            try
            {
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    string[] result = dialog.ResultFileContent.Split('\n');

                    int num_of_correct_answer = 0;
                    for (int i = 0; i < DataGridViewAnswerTable.Rows.Count - 1; i++)
                    {
                        result[i] = result[i].Replace('\n', ' ').Trim();
                        if (("" + DataGridViewAnswerTable.Rows[i].Cells[1].Value).ToUpper() == result[i].ToUpper()) num_of_correct_answer++;
                        DataGridViewAnswerTable.Rows[i].Cells[2].Value = result[i];
                    }

                    TextBoxNumOfCorrectAnswer.Text = "" + num_of_correct_answer;
                    TextBoxNumOfWrongAnswer.Text = "" + (50 - num_of_correct_answer);
                }
            ;
            }
            catch { MessageBox.Show("File đáp án lỗi"); }



        }

        private void DataGridViewAnswerTable_CellClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
