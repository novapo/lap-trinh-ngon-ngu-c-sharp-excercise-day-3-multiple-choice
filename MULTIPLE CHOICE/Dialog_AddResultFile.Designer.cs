﻿namespace bai2cs
{
    partial class Dialog_AddResultFile
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ButtonChooseFile = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.TextBoxFilePath = new System.Windows.Forms.TextBox();
            this.ButtonAgree = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // ButtonChooseFile
            // 
            this.ButtonChooseFile.Location = new System.Drawing.Point(487, 24);
            this.ButtonChooseFile.Margin = new System.Windows.Forms.Padding(4);
            this.ButtonChooseFile.Name = "ButtonChooseFile";
            this.ButtonChooseFile.Size = new System.Drawing.Size(100, 28);
            this.ButtonChooseFile.TabIndex = 15;
            this.ButtonChooseFile.Text = "Chọn file";
            this.ButtonChooseFile.UseVisualStyleBackColor = true;
            this.ButtonChooseFile.Click += new System.EventHandler(this.ButtonChooseFile_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(51, 30);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 17);
            this.label1.TabIndex = 13;
            this.label1.Text = "Tên file";
            // 
            // TextBoxFilePath
            // 
            this.TextBoxFilePath.Location = new System.Drawing.Point(114, 27);
            this.TextBoxFilePath.Margin = new System.Windows.Forms.Padding(4);
            this.TextBoxFilePath.Name = "TextBoxFilePath";
            this.TextBoxFilePath.Size = new System.Drawing.Size(365, 22);
            this.TextBoxFilePath.TabIndex = 11;
            // 
            // ButtonAgree
            // 
            this.ButtonAgree.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ButtonAgree.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.ButtonAgree.Location = new System.Drawing.Point(258, 67);
            this.ButtonAgree.Name = "ButtonAgree";
            this.ButtonAgree.Size = new System.Drawing.Size(128, 34);
            this.ButtonAgree.TabIndex = 16;
            this.ButtonAgree.Text = "Đồng ý";
            this.ButtonAgree.UseVisualStyleBackColor = true;
            this.ButtonAgree.Click += new System.EventHandler(this.ButtonAgree_Click);
            // 
            // Dialog_AddResultFile
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(639, 125);
            this.Controls.Add(this.ButtonAgree);
            this.Controls.Add(this.ButtonChooseFile);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.TextBoxFilePath);
            this.Name = "Dialog_AddResultFile";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Chọn file đáp án";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button ButtonChooseFile;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox TextBoxFilePath;
        private System.Windows.Forms.Button ButtonAgree;
    }
}